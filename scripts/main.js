
// dropdown logic
$('.nav-item').on('click', function(e) {
  
    $(this).toggleClass('open');
});
// function addOrRemoveFillBg() {
//     $(window).scrollTop() > 0 ? (
//       $(".navbar.fixed-top").removeClass("bg-transparent").addClass("bg-white")
//     ) : (
//         $(".navbar.fixed-top").removeClass("bg-white").addClass("bg-transparent")
//       )
//   }

// add background white to navbar for small sized screens
function fillbgForSmallScreens() {
    window.innerWidth < 768 && ($(".navbar.fixed-top")).addClass("bg-white").removeClass("bg-transparent");
}
$(window).resize(function () { 
    fillbgForSmallScreens();
});

// hamburger logic
var forEach = function (t, o, r) {
    if ("[object Object]" === Object.prototype.toString.call(t)) for (var c in t) Object.prototype.hasOwnProperty.call(t, c) && o.call(r, t[c], c, t);
    else for (var e = 0, l = t.length; l > e; e++)o.call(r, t[e], e, t)
  };
var hamburgers = document.querySelectorAll(".hamberger");
      if (hamburgers.length > 0) {
        forEach(hamburgers, function (hamburger) {
          hamburger.addEventListener("click", function () {
            this.classList.toggle("is-active");
          }, false);
        });
      }